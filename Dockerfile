FROM docker.io/nethermindeth/nethermind:1.13.2-test-deb
USER root

ADD start.sh /
ADD bootnode /nethermind
RUN chmod +x /start.sh

ENTRYPOINT ["/start.sh"]
